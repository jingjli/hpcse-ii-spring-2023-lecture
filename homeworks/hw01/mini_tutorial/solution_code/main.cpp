#include <iostream>
#include <vector>
#include <mpi.h>
#include <omp.h>

void matrix_multiply(const std::vector<double>& A, const std::vector<double>& B,
                     std::vector<double>& C, int size)
{
    #pragma omp parallel for collapse(2)
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            double sum = 0.0;
            for (int k = 0; k < size; k++) {
                sum += A[i*size + k] * B[k*size + j];
            }
            C[i*size + j] = sum;
        }
    }
}

int main(int argc, char** argv)
{
    int rank, num_procs;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

    #pragma omp parallel
    {
        #pragma omp critical
        std::cout << "Hello! I am MPI rank[" << rank << "] with omp thread[" << omp_get_thread_num() << "]\n"; 
    }


    int size = 100;

    // Allocate matrices A and B on each process
    std::vector<double> A(size * size);
    std::vector<double> B(size * size);
    std::vector<double> C(size * size);

    // Initialize matrices A and B on each process
    #pragma omp parallel for
    for (int i = 0; i < size * size; i++) {
        //A[i] = 1.0 + rank;
        //B[i] = 1.0 + rank;

        A[i] = 1.0/(i + rank);
        B[i] = 1.0/(i + rank);
    }

    if(rank >= 0 && size < 4){
        std::cout << "Before matrix multiplication:\n";

        std::cout << "A:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << A[i] << " ";
        }
        std::cout << "\n";

        std::cout << "B:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << B[i] << " ";
        }
        std::cout << "\n";

        std::cout << "C:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << C[i] << " ";
        }
        std::cout << "\n";
    }

    // Perform matrix multiplication on each process
    const int reps = 10;

    double time = MPI_Wtime();
    for(int i = 0; i < reps; i++){
        matrix_multiply(A, B, C, size);
    }
    time = MPI_Wtime() - time;

    // Gather the results on the root process
    MPI_Allreduce(MPI_IN_PLACE, C.data(), size*size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    if(size < 4){
        std::cout << "C:\n";
        for (int i = 0; i < size * size; i++) {
            if(i % size == 0){
                std::cout << "\n";
            }
            std::cout << C[i] << " ";
        }
        std::cout << "\n";
    }

    if(rank == 0){
        std::cout << "Execution time: " << time/double(reps) << " sec\n";
    }

    MPI_Finalize();
    return 0;
}
