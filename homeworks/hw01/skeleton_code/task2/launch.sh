#!/bin/bash -l

#SBATCH --nodes=1                   # modify this accordingly!
#SBATCH --ntasks=1                  # modify this accordingly!
#SBATCH --ntasks-per-node=1         # modify this accordingly!
#SBATCH --output=slurm_output.txt
#SBATCH --error=slurm_error.txt
#SBATCH --time=00:05:00

mpirun ./mainMPI 128 128 128 1 1 1
