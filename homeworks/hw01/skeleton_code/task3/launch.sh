#!/bin/bash -l

#SBATCH --nodes=1                   # modify this accordingly!
#SBATCH --ntasks=4                  # modify this accordingly!
#SBATCH --ntasks-per-node=4         # modify this accordingly!
#SBATCH --output=slurm_output.txt
#SBATCH --error=slurm_error.txt
#SBATCH --time=00:10:00

export OMP_NUM_THREADS=2

mpirun -bind-to core --map-by core ./main 256 1.0

